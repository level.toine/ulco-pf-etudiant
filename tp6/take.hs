myTake2  :: [Int] -> [Int] 
myTake2 (xs) = take 2 xs

myget2 :: [Int] -> Int
myget2 = flip (!!) 2

main::IO()
main=do
    print(myTake2 [0..8])
    print(myget2 [1..4])
    print(map (2*)[1..4])
    print(map ((*)2)[1..4])
    print(map (*2)[1..4])
    print(map (div 2)[1..4])
    print(map((flip div)2)[1..4])
    print(map (div 2)[1..4])