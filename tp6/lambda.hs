main::IO()
main=do
    putStrLn "Lambda"
    print (map (\x -> x*2)[1..4::Int])
    print (map (\x -> x `div` 2) [1..4])
    print (map (\ (c,i) -> [c] ++ "-" ++ show i) (zip ['a' .. 'z'] [1 .. 26]))