{-# LANGUAGE OverloadedStrings #-}

import qualified GI.Gtk as Gtk
--import Data.Text

handdleButton::IO()
handdleButton = putStrLn "Hello"

main :: IO ()
main = do
    _ <- Gtk.init Nothing
    window <- Gtk.windowNew Gtk.WindowTypeToplevel 
    Gtk.windowSetDefaultSize window 300 100
    Gtk.windowSetTitle window "Hello World!"
    _ <- Gtk.onWidgetDestroy window Gtk.mainQuit

    --label <- Gtk.labelNew (Just "Hello World!")
    --Gtk.containerAdd window label

    buttonAdd <- Gtk.buttonNewWithLabel "ADD"
    Gtk.containerAdd window buttonAdd
    _ <- Gtk.onButtonClicked buttonAdd handdleButton

    --buttonClear <- Gtk.buttonNewWithLabel "Clear"
    --Gtk.containerAdd window buttonClear
    --_ <- Gtk.onButtonClicked buttonClear handdleButton

    Gtk.widgetShowAll window

    Gtk.main

