maDoubler1 :: [Int] -> [Int]
maDoubler1 [] = []
maDoubler1 (x:xs) = x*2 : maDoubler1 xs

maDoubler2 :: [Int] -> [Int]
maDoubler2 [] = []
maDoubler2 xs = map (* 2) xs

mymap :: (Int -> Int) -> [Int] -> [Int]
mymap _ [] = []
mymap f (x:xs) = f x : mymap f xs

main :: IO()
main = do
    print(maDoubler1 [1..4])
    print(maDoubler2 [1..4])
    print(mymap (*2) [1..4])