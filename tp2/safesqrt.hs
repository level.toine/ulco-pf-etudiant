safeSqrt :: Double -> Maybe Double
safeSqrt d
    |d < 0 = Nothing
    |otherwise = Just (sqrt d)

formatSqrt :: Double -> String
formatSqrt d = case safeSqrt d of
    Nothing -> "sqrt(" ++ show d ++ ") is not defined"
    Just n -> "sqrt(" ++ show d ++ ")= " ++ show n

main :: IO()
main=do
    print(safeSqrt 12)
    print(formatSqrt 12)