import Text.Read

main :: IO()
main = do
    val <- getLine
    let x = readMaybe val
    print (x::Maybe Int)
    putStrLn "this is the end"