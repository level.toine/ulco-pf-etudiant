fibo :: Int -> Int
fibo x = case x of
    0 -> 0
    1 -> 1
    x -> fibo(x-1) + fibo(x-2)

main :: IO()
main = do
    print(fibo 9)