import Text.Read
import System.Environment
import System.IO

type Task = (Int, Bool, String)     -- id, done?, description
type Todo = (Int, [Task])           -- nextId, tasks

afficherCmd = do
    putStrLn "usage: \n\
                \print \n\
                \print todo \n\
                \print done \n\
                \add <string> \n\
                \do <int> \n\
                \undo <int> \n\
                \del <int> \n\
                \exit "

afficherPrint::Task -> IO()
afficherPrint (idtask,done,libelle)
    | done = do
        putStrLn $ "[X] " ++ (show idtask) ++ ". " ++ libelle
    | otherwise = do
        putStrLn $ "[-] " ++ (show idtask) ++ ". " ++ libelle

afficherTodo::Task -> IO()
afficherTodo (idtask,done,libelle) = if done == False then putStrLn $ "[-] " ++ (show idtask) ++ ". " ++ libelle else  putStrLn ""

afficherDone::Task -> IO()
afficherDone (idtask,done,libelle) = if done == True then putStrLn $ "[X] " ++ (show idtask) ++ ". " ++ libelle else  putStrLn ""

    {-
addListe:: Int -> IO()
addListe idnext = do
    putStrLn ("entrer un id")
    id <- getLine
    putStrLn ("entre un texte")
    libelle <- getLine
    putStrLn ("("++ idnext+1 ++ "["++ id ++ ",False," ++ libelle ++ "])")
    writeFile "tasks.txt" ("("++ idnext+1 ++ "["++ id ++ ",False," ++ libelle ++ "])")

doListe::Task -> IO()
doListe = do
    idtodo <- getLine
doListe (id, done, libelle ) = if idtodo == id then putStrLn ("changer l'état en fait") else putStrLn ("")

undoListe::Task->IO()
id <- getLine
undoListe (idtodo, done, libelle) = if idtodo == id then unset (idtodo, done, libelle) else putStrLn ("")


undo::Task->IO()
id <- getLine
suppTask (idtodo, done, libelle) = if idtodo == id then del (idtodo, done, libelle) else putStrLn ("")
-}

main :: IO()
main = do
    contents <- readFile "tasks.txt"
    let todo1 = read $! contents :: Todo
    cmd <- getLine
    case cmd of
        "print" -> mapM_ afficherPrint (snd todo1)
        "print todo" -> mapM_ afficherTodo (snd todo1)
        "print done" -> mapM_ afficherDone (snd todo1)
        --"add" -> addListe idnext
        --"do" -> mapM_ doListe (snd todo1)  
        --"undo" -> undoListe
        --"del" -> suppTask 
        otherwise -> afficherCmd

