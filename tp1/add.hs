add2Int :: Int->Int->Int
add2Int x y = x+y

add2Double :: Double -> Double -> Double 
add2Double x y = x+y

add2Num :: Num a => a -> a -> a
add2Num x y = x + y

main = do
    print (add2Int 12 30)
    print (add2Double 12 30)
