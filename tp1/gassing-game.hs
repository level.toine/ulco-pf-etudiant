import System.Random

loop::Int->Int->IO()
loop 0 num = putStrLn "fin"
loop n num = do
    putStr("choisissez un nombre : ")
    msg<-getLine
    let n = read msg
    if n == num
        then putStr "vous avez trouvé bravo"
    else do
    if n < num
        then putStrLn ("le nombre est plus grand")
        else putStrLn ("le nombre est plus petit")
    loop (n-1) num

main :: IO()
main = do
    num <- randomRIO (0,100::Int)
    print num
    loop 10 num