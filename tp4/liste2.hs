mulListe :: Num a => a -> [a] -> [a]
mulListe _ [] = []
mulListe n (x : xs) = (n * x) : (mulListe n xs)


selectListe :: Ord a => (a, a) -> [a] -> [a]
selectListe _ [] = []
selectListe (xMin,xMax) (x:xs) = 
    if x <= xMax && x >= xMin  
        then x : selectListe (xMin, xMax) xs
    else
        selectListe (xMin, xMax) xs

sumListe :: Num a => [a] -> a
sumListe [] = 0
sumListe (x : xs) = x + sumListe xs
        
sumListeD :: [Double] -> Double
sumListeD [] = 0
sumListeD (x : xs) = x + sumListeD xs

main :: IO()
main = do
    print (mulListe 2 [21, 3])
   