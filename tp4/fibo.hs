import System.Environment

fibo::Int->Int
fibo 0 = 0
fibo 1 = 1
fibo n = fibo(n-1)+fibo(n-2)

fiboTco :: Int -> Int
fiboTco x = local x 0 1
        where local 0 a _ = a
              local 1 _ b = b
              local n a b = local (n-1) b (a+b)

main :: IO ()
main = do
    args <- getArgs
    case args of
        ["naive", nStr] -> print (fibo (read nStr))
        ["tco", nStr] -> print (fiboTco (read nStr))
        _ -> putStrLn "usage: <naive|tco> <n>"