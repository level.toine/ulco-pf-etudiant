import Data.Char

myLength :: [a] -> Int
myLength [] = 0
myLength (_ : xs) = 1 + myLength xs

toUpperString :: [Char] -> [Char]
toUpperString [] = []
toUpperString (x : xs) = (toUpper x) : (toUpperString xs)

onlyLetters :: [Char] -> [Char]
onlyLetters [] = []
onlyLetters (x : xs) =
  if isLetter x
    then x : onlyLetters xs
    else onlyLetters xs

main::IO()
main = do
    print (myLength [1 .. 4::Int])
    print (myLength "madaradona")